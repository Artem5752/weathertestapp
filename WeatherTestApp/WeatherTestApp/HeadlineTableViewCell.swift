//
//  HeadlineTableViewCell.swift
//  WeatherTestApp
//
//  Created by Artem Franchuk on 30.08.2018.
//  Copyright © 2018 Artem Franchuk. All rights reserved.
//

import UIKit

struct Headline {
    
}

class HeadlineTableViewCell: UITableViewCell {
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var imageViewWeather: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
