//
//  WeatherTableViewController.swift
//  WeatherTestApp
//
//  Created by Artem Franchuk on 28.08.2018.
//  Copyright © 2018 Artem Franchuk. All rights reserved.
//

import UIKit

class WeatherTableViewController: UITableViewController, UISearchBarDelegate  {
    
    class SpinnerViewController: UITableViewController {
        var spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        
        override func loadView() {
            view = UIView()
            view.backgroundColor = UIColor(white: 0, alpha: 0.1)
            
            
            spinner.translatesAutoresizingMaskIntoConstraints = false
            spinner.startAnimating()
            view.addSubview(spinner)
            
            spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        }
    }
    
    var cityData:Array< String > = Array < String >()
    var tempData: Array< String > = Array < String >()
    var iconData: Array< String > = Array < String >()
    
    
    @IBOutlet weak var addButtonOut: UIBarButtonItem!
    
    
    fileprivate struct ErrorData : Decodable {
        let cod : Int
        let message: String
    }
    
    fileprivate struct City: Decodable{
        let cod:Int
        let name:String
        let id: Int
        let sys: Sys?
        let dt: Int
        let clouds: Clouds?
        let wind: Winds?
        let main: Mains?
        let visibility: Int?
        let base: String
        let weather: [Weather]?
        let coord: Coords?
    }
    
    fileprivate struct Sys:Decodable{
        let type:Int
        let id:Int
        let message:Double
        let country:String
        let sunrise:Int
        let sunset:Int
    }
    
    fileprivate struct Clouds: Decodable{
        let all: Int
    }
    
    fileprivate struct Winds: Decodable{
        let speed: Int
        let deg: Int
    }
    
    fileprivate struct Mains:Decodable{
        let temp: Double?
        let pressure:  Int
        let humidity: Int
        let temp_min: Double
        let temp_max: Double
        
    }
    
    fileprivate struct Weather: Decodable{
        let id: Int
        let main: String
        let description: String
        let icon: String
    }
    
    //    struct Zero: Decodable{
    //        let id: Int
    //        let main: String
    //        let description: String
    //        let icon: String
    //    }
    
    fileprivate struct Coords: Decodable{
        let lon: Double
        let lat: Double
    }
    
    
    @IBOutlet weak var citySearchBar: UISearchBar!
    typealias JSONPayload = [String : Any]
    
    public var stringURL: String = ""
    let city: String! = "Kyiv"
    let apiID: String = "1769088dad71fa02b08a8edd78200539"
    var cityN: String = ""
    var tempN: String = ""
    var iconN: String = ""
    var codN: String = "404"
    var someError: Int = 0
    
    //    let lang: String = "ua"
    //    let metric: String = "metric"
    
    var exists: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        citySearchBar.delegate = self
        //createSpinnerView()
        //        tableView.dataSource = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    let child = SpinnerViewController()
    
    
    func createSpinnerView() {
        
        // add the spinner view controller
        addChildViewController(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
        
        // wait two seconds to simulate some work happening
        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        // then remove the spinner view controller
        //self.deleteSpinnerView()
        // }
    }
    func deleteSpinnerView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.child.willMove(toParentViewController: nil)
            self.child.view.removeFromSuperview()
            self.child.removeFromParentViewController()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let cityURL = searchBar.text, !cityURL.isEmpty{
            stringURL = "https://api.openweathermap.org/data/2.5/weather?q=\(cityURL)&units=metric&APPID=\(apiID)"
            print(stringURL)
            
        } else {
            //alert
            //            let alert = UIAlertController(title: "Error", message: "Please enter city", preferredStyle: UIAlertControllerStyle.alert)
            //            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            //                switch action.style{
            //                case .default:
            //                    print("default")
            //
            //                case .cancel:
            //                    print("cancel")
            //
            //                case .destructive:
            //                    print("destructive")
            //
            //
            //                }}))
            //            self.present(alert, animated: true, completion: nil)
            //            addButtonOut.isEnabled = false
        }
        //start example
        guard let url = URL(string: stringURL) else {return}
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        createSpinnerView()
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else {return}
            
            
            do{
                //spinners
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.deleteSpinnerView()
                
                //decodable
                let city = try JSONDecoder().decode(City.self, from: data)
                self.codN = String(describing: city.cod)
                self.cityN = city.name
                self.tempN = String(format: "%.1f", (city.main?.temp)!)
                self.iconN = String(describing: city.weather?.first?.icon as! String)
                print(city.name)
                //print("Clouds: \(String(describing: city.clouds))")
                //print("Winds: \(String(describing: city.wind))")
                //print("Temp: \(String(describing: city.main))")
                print("Weather: \(String(describing: city.weather?.first?.icon))")
                //self.tempN = (city.main?.temp)!
                //let tempN:String = String(format: "%f", (city.main?.temp)!)
                print("Temperature: \(self.tempN)")
                print ("Some city cod: \(self.codN)")
                
                DispatchQueue.main.async {
                    if self.codN == "404" {
                        self.addButtonOut.isEnabled = false
                        self.someError = 0
                    } else
                        //(self.codN == "404")
                    {
                        //self.someError = 1
                        self.addButtonOut.isEnabled = true
                        // self.someDispatch()
                        
                    }
                }
                
                print (city.base)
                
                
                
                
                
                
                // swiwt 2/3/ObjC
                //                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                // print(json)
            } catch let jsonErr{
                print("Error serializing JSON:\(jsonErr)")
                self.someError = 1
            }
            //            } catch DecodingError.typeMismatch {
            //                let container = try decoder.container(keyedBy: ErrorData.self)
            //                cod = try container.decodeIfPresent(String.self, forKey: .cod)
            //                message = try container.decodeIfPresent(String.self, forKey: .message)
            //
            //                alertForCheckAddButton()
            //            }
            
            
            }.resume()
        
        
        
        
        
        
    }
    
    
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    func alertForCheckAddButton(){
        
        let alert = UIAlertController(title: "Error", message: "Please enter city", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
        self.addButtonOut.isEnabled = false
    }
    
    func someDispatch(){
        DispatchQueue.main.async {
            self.alertForCheckAddButton()
        }
    }
    
    
    @IBAction func addCity(_ sender: Any) {
        
        if cityData.contains(cityN){
            print("We have that city in the list")
            //self.someDispatch()
        }
            //        else if tempData.contains(tempN){
            //            print("We have that temperature in the list")
            //        }
        else{
            cityData.append(cityN)
            print("Add new element: \(cityN)")
            tempData.append(tempN as String)
            print("Add new element: \(tempN)")
            iconData.append(iconN as String)
            print("Add new element: \(iconN)")
        }
        print("Array of city: \(self.cityData.count)")
        print("Array of temp: \(self.tempData.count)")
        print("Array of temp: \(self.iconData.count)")
        tableView.reloadData()
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tempData.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HeadlineTableViewCell
        //cell.textLabel?.text = tableData[indexPath.row]
        //cityLabel.text = tableData[indexPath.row]
        //cell.detailTextLabel?.text = tempData[indexPath.row]
        //tempLabel.text = tempData[indexPath.row]
        // Configure the cell...
        
        //let headline = UIImage[indexPath.row]
        var iconImage = "\(iconData[indexPath.row])"
        
        cell.cityLabel?.text = cityData[indexPath.row]
        cell.tempLabel?.text = tempData[indexPath.row]
        cell.imageViewWeather?.image = UIImage(named: iconImage)
        
        return cell
        
    }
    
    
    
    
    //   Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            cityData.remove(at: indexPath.row)
            tempData.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath],  with: UITableViewRowAnimation.automatic)
            //iconData.remove(at: indexPath.row)
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
